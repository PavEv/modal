import "./assets/css/style.css";

(function(window) {
  const _init = ({ contentSelector, openSelector }) => {
    let modalElement = null;
    let overlayElement = null;
    let contentElement = null;

    let animationDuration = 300;
    const cssNames = {
      scrollHidden: "scroll-hidden",
      modal: "modal",
      modalHidden: "modal_hidden",
      modalVisible: "modal_visible",
      modalVisibleAnimation: "modal_visible_animate",
      modalHiddenAnimation: "modal_hidden_animate",
      modalOverlay: "modal__overlay",
      modalContent: "modal__content"
    };

    const keys = {
      esc: 27
    };

    const openModal = () => {
      document.body.classList.add(cssNames.scrollHidden);

      modalElement.classList.add(cssNames.modalVisible);
      modalElement.classList.add(cssNames.modalVisibleAnimation);
      modalElement.focus();

      setTimeout(() => {
        modalElement.classList.remove(cssNames.modalHidden);
        modalElement.classList.remove(cssNames.modalVisibleAnimation);
      }, animationDuration);
    };

    const closeModal = () => {

      modalElement.blur();
      modalElement.classList.add(cssNames.modalHiddenAnimation);

      setTimeout(() => {
      document.body.classList.remove(cssNames.scrollHidden);
        modalElement.classList.add(cssNames.modalHidden);
        modalElement.classList.remove(cssNames.modalHiddenAnimation);
        modalElement.classList.remove(cssNames.modalVisible);
      }, animationDuration);
    };

    const generateModalElement = oldContentElement => {
      const rootModalElement = document.createElement("div");
      rootModalElement.className = `${cssNames.modal} ${cssNames.modalHidden}`;
      rootModalElement.tabIndex = 0;

      const overlayModalElement = document.createElement("div");
      overlayModalElement.className = cssNames.modalOverlay;

      const contentModalElement = document.createElement("div");
      contentModalElement.className = cssNames.modalContent;

      contentModalElement.appendChild(oldContentElement.cloneNode(true));
      overlayModalElement.appendChild(contentModalElement);
      rootModalElement.appendChild(overlayModalElement);

      overlayElement = overlayModalElement;
      modalElement = rootModalElement;
      contentElement = contentModalElement;
    };

    document
      .getElementsByClassName(openSelector)[0]
      .addEventListener("click", openModal);

    const oldContentElement = document.getElementsByClassName(
      contentSelector
    )[0];
    const parentContentElement = oldContentElement.parentNode;

    generateModalElement(oldContentElement);

    contentElement.addEventListener("click", event => {
      event.stopPropagation();
    });

    parentContentElement.replaceChild(modalElement, oldContentElement);

    overlayElement.addEventListener("click", closeModal);

    modalElement.addEventListener("keydown", ({ keyCode, which }) => {
      var key = keyCode || which;
      if (key === keys.esc) closeModal();
    });

    return {
      closeModal
    };
  };

  window.Modal = {
    init: _init
  };
})(window);
